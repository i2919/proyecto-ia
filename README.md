<h1>Clasificador de Covid-19 y Neumonía.<h1>

![Banner_Proyecto](Banner_Proyecto.jpg?raw=true "Banner Proyecto")

**Autores**: Daniel Alejandro Castillo Rodriguez, Julian Camilo Camacho Torres, Juan Sebastián Díaz Gutiérrez

**Objetivo**: Clasificar a partir de imagenes de rayos X toraxicas entre Covid-19 y Neumonía.

[**Dataset1**](https://www.kaggle.com/tawsifurrahman/covid19-radiography-database), [**Dataset2**](https://www.kaggle.com/amanullahasraf/covid19-pneumonia-normal-chest-xray-pa-dataset): Dos datasets con aproximadamente en conjunto 25000 imagenes de rayos x clasificadas en 4 categorias

**Librerias**: Pandas, Numpy, Pytorch, Matploplib, cv2, Tensorflow

**Modelos**: Resnet, Alexnet, VGG, Squeezenet, Densenet, Inception, MobileNetV3, ChexNet

[**Notebook**](Proyecto_IA2.ipynb)

[**Video**](https://www.youtube.com/watch?v=GRjDea5lry0)

[**Diapositivas**](Presentación_Proyecto.pdf)

[**Repositorio**](https://gitlab.com/i2919/proyecto-ia)
